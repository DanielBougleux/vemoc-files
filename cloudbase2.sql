-- MariaDB dump 10.17  Distrib 10.4.15-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: cloudbase2
-- ------------------------------------------------------
-- Server version	10.4.15-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `application`
--

DROP TABLE IF EXISTS `application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application` (
  `appid` int(11) NOT NULL AUTO_INCREMENT,
  `appname` varchar(30) NOT NULL,
  `apptype` enum('BATCH','ONLINE') NOT NULL DEFAULT 'BATCH',
  `image` varchar(30) NOT NULL,
  `min_memory` bigint(20) unsigned NOT NULL DEFAULT 1073741824,
  `max_memory` bigint(20) unsigned NOT NULL DEFAULT 4294967296,
  `num_cores` int(11) NOT NULL DEFAULT 1,
  `comments` text DEFAULT NULL,
  PRIMARY KEY (`appid`),
  UNIQUE KEY `appname` (`appname`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `container`
--

DROP TABLE IF EXISTS `container`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `container` (
  `containerid` int(11) NOT NULL AUTO_INCREMENT,
  `rid` int(11) NOT NULL,
  `aid` int(11) NOT NULL,
  `containername` varchar(30) NOT NULL,
  `command` tinytext NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `estimated_time` time NOT NULL,
  `status` enum('NEW','CREATED','QUEUED','RUNNING','PAUSED','SUSPENDING','SUSPENDED','RESUMING','MIGRATING','FINISHED','STOPPED','ERROR') DEFAULT 'NEW',
  PRIMARY KEY (`containerid`),
  UNIQUE KEY `containername` (`containername`),
  KEY `request_fk` (`rid`),
  KEY `application_fk` (`aid`),
  CONSTRAINT `application_fk` FOREIGN KEY (`aid`) REFERENCES `application` (`appid`),
  CONSTRAINT `request_fk` FOREIGN KEY (`rid`) REFERENCES `request` (`reqid`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=185 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `container_history`
--

DROP TABLE IF EXISTS `container_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `container_history` (
  `historyid` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL,
  `logdata` blob DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`historyid`),
  KEY `container_fk` (`cid`),
  CONSTRAINT `container_fk` FOREIGN KEY (`cid`) REFERENCES `container` (`containerid`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22164 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `host`
--

DROP TABLE IF EXISTS `host`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `host` (
  `hostid` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(50) NOT NULL,
  `hostdata` mediumblob DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`hostid`),
  UNIQUE KEY `hostname` (`hostname`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `request`
--

DROP TABLE IF EXISTS `request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `request` (
  `reqid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `reqname` varchar(30) NOT NULL,
  `submit_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `reqstatus` enum('NEW','QUEUED','SCHEDULED','RUNNING','FINISHED','ERROR') NOT NULL DEFAULT 'NEW',
  `num_containers` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`reqid`),
  KEY `user_fk` (`uid`),
  CONSTRAINT `user_fk` FOREIGN KEY (`uid`) REFERENCES `user` (`userid`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(30) NOT NULL,
  `password` varchar(41) NOT NULL,
  `username` varchar(50) NOT NULL,
  `usertype` enum('USER','ADMIN') NOT NULL DEFAULT 'USER',
  PRIMARY KEY (`userid`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-05 22:11:32
