#!/bin/bash

echo "Instalando Python 3.6...." && echo " " && echo " "
echo " " && echo " "


echo "Atualizando os pacotes...." && echo " " && echo " "
sudo yum update -y
echo " " && echo " "

echo "Instalando o Docker...." && echo " " && echo " "
sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo -y
sudo yum install docker-ce docker-ce-cli containerd.io -y
sudo systemctl start docker
sudo systemctl enable docker
echo " " && echo " "

echo "Instalando Python 3.6...." && echo " " && echo " "
sudo yum install gcc
sudo yum install python3-devel -y
sudo yum install python3-pip -y
echo " " && echo " "

echo "Instalando pacotes do pip...." && echo " " && echo " "
sudo pip3 install docker
sudo pip3 install dill
sudo pip3 install mysql-connector-python
sudo pip3 install psutil
sudo pip3 install pymongo
sudo pip3 install jsonpickle
echo " " && echo " "

echo "Instalando Pacotes do VEMoC...." && echo " " && echo " "
sudo yum install dnf -y
sudo yum install dnf-plugins-core -y
sudo dnf copr ganto/umoci -y
sudo dnf install umoci -y

sudo yum install skopeo -y

sudo yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo yum install jq -y

sudo yum -y install epel-release
sudo yum -y install debootstrap perl libvirt libcap-devel libcgroup wget bridge-utils
sudo yum -y install lxc lxc-templates lxc-extra
systemctl start lxc.service
systemctl enable lxc.service

sudo yum install python36-lxc

sudo wget https://dev.mysql.com/get/mysql80-community-release-el7-3.noarch.rpm
sudo rpm -Uvh mysql80-community-release-el7-3.noarch.rpm
sudo yum install mysql-server -y
sudo systemctl start mysqld
sudo systemctl enable mysqld

#instalar o mongodb https://www.digitalocean.com/community/tutorials/how-to-install-mongodb-on-centos-7

sudo yum install git -y
echo " " && echo " "

echo "preparando arquivo de swap...." && echo " " && echo " "
sudo dd if=/dev/zero of=/swapfile bs=512M count=8
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
sudo su -c "echo /swapfile swap swap defaults 0 0 >> /etc/fstab"

echo "clonando repositorio do git do VEMoC..." && echo " " && echo " "
sudo git clone https://github.com/Zilves/elasticcontainer.git
echo " " && echo " "

sudo mkdir /opt/templates
sudo reboot